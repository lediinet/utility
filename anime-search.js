console.log("Loading LediiNet anime-search.js...");

const RATING_COLORS = [ " common", "uncommon", "rare", "epic", "legendary" ];
const SEARCH_CLASS = "anime-search-hidden";
const PREFIX_TITLE = "t:"
const PREFIX_DESCRIPTION = "d:"
const PREFIX_COMMENT = "c:"
const PREFIX_GENRES = "g:"
const PREFIX_RATING = "s:"



//Setup searching fields
var cachedAnimeElements = null;
var isSearching = false;
var searchInput = document.getElementById("ledii-search");
searchInput.onchange = function() { onSearchChanged(); }
var firstOrderIndex = 0;
var searchOrder = document.getElementById("ledii-search-order");
searchOrder.onchange = function() { onSearchOrderChanged(); }

setupAnimeElements();
onSearchOrderChanged();



//Gather information about anime section elements
function getAllAnimeElements() {
    if (cachedAnimeElements) { return cachedAnimeElements; }

    var sections = document.querySelectorAll("section");
    cachedAnimeElements = [];
    
    sections.forEach(function(section) {
        var animeElements = getAnimeElements(section);
        if (animeElements) {
            cachedAnimeElements.push(animeElements);
        }
    });
    console.log("Initialized cache for " + cachedAnimeElements.length + " anime sections...");
    return cachedAnimeElements;
}

function getAnimeElements(section) {
    var title = section.querySelector(".sqsrte-large");
    if (title == null) { return null; }
    
    var paragraphs = section.querySelectorAll("p");
    var elements = [];
    elements["section"] = section;
    elements["title"] = title;
    elements["description"] = paragraphs[1];
    elements["comment"] = paragraphs[2];
    if (!elements["comment"]) { elements["comment"] = elements["description"]; }
    elements["genres"] = section.querySelector(".genre");
    elements["episodes"] = section.querySelector(".episodes");
    elements["rating"] = section.querySelector(".star-rating");
    return elements;
}



//Functions for search ordering
function onSearchOrderChanged() {
    var orderType = searchOrder.value.toLowerCase();
    //console.log("Search order: " + orderType);
    
    switch (orderType) {
        case "date-added-down": { sortByAddedDate(false); break; }
        case "sum-rating-down": { sortBySumRating(false); break; }
        case "star-count-down": { sortByStarCount(false); break; }
        case "star-color-down": { sortByStarColor(false); break; }
        case "episodes-down": { sortByEpisodes(false); break; }
        
        case "date-added-up": { sortByAddedDate(true); break; }
        case "sum-rating-up": { sortBySumRating(true); break; }
        case "star-count-up": { sortByStarCount(true); break; }
        case "star-color-up": { sortByStarColor(true); break; }
        case "episodes-up": { sortByEpisodes(true); break; }
    }
}

function setupAnimeElements() {
    var sectionParent = document.querySelector("#sections");
    var sectionSiblings = Array.from(sectionParent.children);
    var allAnimeElements = getAllAnimeElements();
    
    //Setup default order id
    var currentId = 1;
    allAnimeElements.forEach((animeElements) => {
        if (currentId == 1) {
            firstOrderIndex = sectionSiblings.indexOf(animeElements["section"]);
            //console.log("First order index was set to <" + firstOrderIndex + ">");
        }
        animeElements["section"].setAttribute("default-order-id", currentId);
        
        //Setup episode count
        searchForEpisodes(animeElements);
    
        currentId++;
    });
}

function sortByAddedDate(inverted) {
    //console.log("Sorting by added date...");
    
    var allAnimeElements = getAllAnimeElements();
    var sortedAnimeElements = Array.from(allAnimeElements).sort(function(elementsA, elementsB) {
        var valueA = parseInt(elementsA["section"].getAttribute("default-order-id"));
        var valueB = parseInt(elementsB["section"].getAttribute("default-order-id"));
        return valueA < valueB ? 1 : valueA > valueB ? -1 : 0;
    });
    
    applySortingOrder(sortedAnimeElements, inverted);
}

function sortBySumRating(inverted) {
    //console.log("Sorting by overall rating...");
    const COLOR_WEIGHT = 1.25;
    const COUNT_WEIGHT = 1.0;
    
    var allAnimeElements = getAllAnimeElements();
    var sortedAnimeElements = Array.from(allAnimeElements).sort(function(elementsA, elementsB) {
        var valueA = getRatingCountIndex(null, elementsA["rating"].className.toLowerCase()) * COUNT_WEIGHT;
        var valueB = getRatingCountIndex(null, elementsB["rating"].className.toLowerCase()) * COUNT_WEIGHT;
        valueA += getRatingColorIndex(null, elementsA["rating"].className.toLowerCase()) * COLOR_WEIGHT;
        valueB += getRatingColorIndex(null, elementsB["rating"].className.toLowerCase()) * COLOR_WEIGHT;
        return valueA < valueB ? 1 : valueA > valueB ? -1 : 0;
    });
    
    applySortingOrder(sortedAnimeElements, inverted);
}

function sortByStarCount(inverted) {
    //console.log("Sorting by star count...");
    
    var allAnimeElements = getAllAnimeElements();
    var sortedAnimeElements = Array.from(allAnimeElements).sort(function(elementsA, elementsB) {
        var valueA = getRatingCountIndex(null, elementsA["rating"].className.toLowerCase());
        var valueB = getRatingCountIndex(null, elementsB["rating"].className.toLowerCase());
        return valueA < valueB ? 1 : valueA > valueB ? -1 : 0;
    });
    
    applySortingOrder(sortedAnimeElements, inverted);
}

function sortByStarColor(inverted) {
    //console.log("Sorting by star color...");
    
    var allAnimeElements = getAllAnimeElements();
    var sortedAnimeElements = Array.from(allAnimeElements).sort(function(elementsA, elementsB) {
        var valueA = getRatingColorIndex(null, elementsA["rating"].className.toLowerCase());
        var valueB = getRatingColorIndex(null, elementsB["rating"].className.toLowerCase());
        return valueA < valueB ? 1 : valueA > valueB ? -1 : 0;
    });
    
    applySortingOrder(sortedAnimeElements, inverted);
}

function sortByEpisodes(inverted) {
    //console.log("Sorting by episodes...");
    
    var allAnimeElements = getAllAnimeElements();
    var sortedAnimeElements = Array.from(allAnimeElements).sort(function(elementsA, elementsB) {
        var valueA = parseInt(elementsA["episodes"].innerText.toLowerCase());
        var valueB = parseInt(elementsB["episodes"].innerText.toLowerCase());
        return valueA < valueB ? 1 : valueA > valueB ? -1 : 0;
    });
    
    applySortingOrder(sortedAnimeElements, inverted);
}

function applySortingOrder(sortedAnimeElements, inverted) {
    var sectionParent = document.querySelector("#sections");
    var tempParent = document.createDocumentFragment();

    if (inverted) {
        sortedAnimeElements.reverse();
    }

    //Moving elements to temp parent...
    sortedAnimeElements.forEach((animeElements) => {
        tempParent.appendChild(animeElements["section"]);
    });
    
    //Moving elements back into main parent...
    var sectionSiblings = Array.from(sectionParent.children);
    var beforeNode = sectionSiblings[firstOrderIndex];
    sectionParent.insertBefore(tempParent, beforeNode);
}



//Functions for search filtering
function onSearchChanged() {
    //console.log("Searched for: " + searchInput.value);
    
    var allAnimeElements = getAllAnimeElements();
    allAnimeElements.forEach((animeElements) => {
        if (evaluateSearch(animeElements)) {
            showSection(animeElements);
        }
        else {
            hideSection(animeElements);
        }
    });
}

function evaluateSearch(animeElements) {
    var searchParams = searchInput.value.toLowerCase().split(" ");
    if (searchParams.length == 0) { return true; }
    
    isSearching = true;
    searchParams.forEach((param) => {
        //console.log("Search param: " + param);
        if (!isSearching) { return false; }
        
        if (param.startsWith(PREFIX_TITLE)) {
            var search = param.substring(PREFIX_TITLE.length);
            if (!evaluateTitle(animeElements, search)) {
                //console.log("Invalid title!");
                isSearching = false;
                return false;
            }
        }
        else if (param.startsWith(PREFIX_DESCRIPTION)) {
            var search = param.substring(PREFIX_DESCRIPTION.length);
            if (!evaluateDescription(animeElements, search)) {
                //console.log("Invalid description!");
                isSearching = false;
                return false;
            }
        }
        else if (param.startsWith(PREFIX_COMMENT)) {
            var search = param.substring(PREFIX_COMMENT.length);
            if (!evaluateComment(animeElements, search)) {
                //console.log("Invalid comment!");
                isSearching = false;
                return false;
            }
        }
        else if (param.startsWith(PREFIX_GENRES)) {
            var search = param.substring(PREFIX_GENRES.length);
            if (!evaluateGenres(animeElements, search)) {
                //console.log("Invalid genres!");
                isSearching = false;
                return false;
            }
        }
        else if (param.startsWith(PREFIX_RATING)) {
            var search = param.substring(PREFIX_RATING.length);
            if (!evaluateRating(animeElements, search)) {
                //console.log("Invalid rating!");
                isSearching = false;
                return false;
            }
        }
        else {
            if (!evaluateAny(animeElements, param)) {
                //console.log("Invalid any!");
                isSearching = false;
                return false;
            }
        }
    });
    
    if (isSearching) {
        isSearching = false;
        //console.log("Valid match!");
        return true;
    }
    return false;
}

function evaluateTitle(animeElements, search) {
    var title = animeElements["title"].innerText.toLowerCase();
    return title.includes(search);
}

function evaluateDescription(animeElements, search) {
    var description = animeElements["description"].innerText.toLowerCase();
    return description.includes(search);
}

function evaluateComment(animeElements, search) {
    var comment = animeElements["comment"].innerText.toLowerCase();
    return comment.includes(search);
}

function evaluateGenres(animeElements, search) {
    var genres = animeElements["genres"].innerText.toLowerCase();
    return genres.includes(search);
}

function evaluateRating(animeElements, search) {
    var rating = animeElements["rating"].className.toLowerCase();
    var includeUp = search.includes("+");
    var includeDown = search.includes("-");
    var count = getRatingCountIndex(rating, search);
    var color = getRatingColorIndex(rating, search);
    
    if (count > 0) {
        return evaluateRatingCount(rating, count, includeUp, includeDown);
    }
    if (color > 0) {
        return evaluateRatingColor(rating, color, includeUp, includeDown);
    }

    return false;
}

function evaluateRatingCount(rating, count, incUp, incDown) {
    var current = count;
    if (incUp) {
        while (current <= 5) {
            if (rating.includes(current)) { return true; }
            current++;
        }
    }
    else if (incDown) {
        while (current > 0) {
            if (rating.includes(current)) { return true; }
            current--;
        }
    }
    return rating.includes(current);
}

function evaluateRatingColor(rating, color, incUp, incDown) {
    var current = color;
    if (incUp) {
        while (current <= 5) {
            if (rating.includes(RATING_COLORS[current-1])) { return true; }
            current++;
        }
    }
    else if (incDown) {
        while (current > 0) {
            if (rating.includes(RATING_COLORS[current-1])) { return true; }
            current--;
        }
    }
    return rating.includes(RATING_COLORS[current-1]);
}

function getRatingCountIndex(rating, search) {
    if (search.includes("1") || search.includes("one")) { return 1; }
    if (search.includes("2") || search.includes("two")) { return 2; }
    if (search.includes("3") || search.includes("three")) { return 3; }
    if (search.includes("4") || search.includes("four")) { return 4; }
    if (search.includes("5") || search.includes("five")) { return 5; }
    return 0;
}

function getRatingColorIndex(rating, search) {
    //Note: Need to check "uncommon" before "common" to avoid string conflict.
    if (search.includes("green") || search.includes("uncommon")) { return 2; }
    if (search.includes("white") || search.includes("common")) { return 1; }
    if (search.includes("blue") || search.includes("rare")) { return 3; }
    if (search.includes("purple") || search.includes("epic")) { return 4; }
    if (search.includes("orange") || search.includes("legendary")) { return 5; }
    return 0;
}

function evaluateAny(animeElements, search) {
    if (evaluateTitle(animeElements, search)) { return true; }
    if (evaluateDescription(animeElements, search)) { return true; }
    if (evaluateComment(animeElements, search)) { return true; }
    if (evaluateGenres(animeElements, search)) { return true; }
    if (evaluateRating(animeElements, search)) { return true; }
    return false;
}

function showSection(animeElements) {
    //console.log("Show section: " + animeElements["title"].innerText);
    var item = animeElements["section"];
    if (item.classList.contains(SEARCH_CLASS)) {
        item.classList.remove(SEARCH_CLASS);
    }
}

function hideSection(animeElements) {
    //console.log("Hide section: " + animeElements["title"].innerText);
    var item = animeElements["section"];
    if (!item.classList.contains(SEARCH_CLASS)) {
        item.classList.add(SEARCH_CLASS);
    }
}

function searchForEpisodes(animeElements) {
    var animeName = animeElements["title"].innerText;
    var animeWiki = animeElements["episodes"].getAttribute("wiki");
    if (animeWiki == null) {
        console.log("Unable to find wiki entry for <" + animeName + ">...");
        return;
    }
    console.log("Searching for episode page for <" + animeName + ">...");
    
    loadWikipediaPage(animeWiki, (animeJson) => {
        console.log("Episode page for <" + animeName + "> is ready.");

        var seasonCount = 0;
        var episodeCount = 0;
        var otherCount = 0;
        var isSpecialTable = false;
        
        const parser = new DOMParser();
        const animeDoc = parser.parseFromString(animeJson.parse.text["*"], "text/html");
        var episodeTables = animeDoc.querySelectorAll(".wikitable");
        episodeTables.forEach(function(table) {
            var tableHeader = findPreviousHeader(table);
            var tableName = (tableHeader) ? tableHeader.innerText.replace("[edit]", "") : "Table";
            var tableCount = 0;
            var tableFirstCount = true;
            var tableRows = table.querySelectorAll("th");
            
            //Expect that all tables are simular until we find 
            if (!isSpecialTable) {
                isSpecialTable = findSpecialTable(table, tableHeader);
            }
            
            tableRows.forEach(function(row) {
                //console.log(row);
                var id = row.id;
                var text = row.innerText;
                if (!id.includes("ep")) { return; }

                //Valid episode found. Count table as season
                if (tableFirstCount) {
                    seasonCount++;
                    tableFirstCount = false;
                }

                //Handle common specials inside tables.
                var number = parseInt(text);
                if (isNaN(number) || text.includes(".")) {
                    otherCount++;
                    isSpecialTable = false;
                    return;
                }
                
                //Assume this is a standard episode
                tableCount++;
            });
            
            //We need to add to the correct count at the end.
            //As finding a special on a table means that it was not actually ONLY specials.
            if (isSpecialTable) {
                otherCount += tableCount;
                console.log("Found new special table <" + tableName + ">");
            }
            else {
                episodeCount += tableCount;
                console.log("Found new episode table <" + tableName + ">");
            }
        });
        
        console.log(episodeCount + " Episodes (" + seasonCount + " Seasons)");

        animeElements["episodes"].innerText = "";
        animeElements["episodes"].appendChild(document.createElement("br"));
        animeElements["episodes"].innerText += episodeCount + " Episodes";
        if (otherCount > 0) {
            animeElements["episodes"].innerText += " + " + otherCount + " Extras";
        }
        //Add extra space before Wiki link
        animeElements["episodes"].innerText += " ";

        var wikiLink = document.createElement("a");
        wikiLink.innerText = "[Wiki]";
        wikiLink.setAttribute("href", "https://en.wikipedia.org/wiki/" + animeWiki);
        wikiLink.setAttribute("target", "_blank");
        animeElements["episodes"].appendChild(wikiLink);
    });
}

function findPreviousHeader(element) {
    var current = element.previousSibling;
    var validTags = ["H1", "H2", "H3", "H4", "H5", "H6"];
    while (current) {
        if (validTags.includes(current.tagName)) {
            return current;
        }
        current = current.previousSibling;
    }
    return null;
}

function findSpecialTable(tableElement, headerElement) {
    if (headerElement == null) {
        return false;
    }
    
    var isFound = false;
    var validKeys = ["OVA", "Special", "Live-action", "CD"];
    var current = tableElement.previousSibling;
    while (true) {
        var text = current.textContent;
        if (text) {
            var searchText = text.toUpperCase();
            validKeys.forEach(function(key) {
                if (isFound) { return; }
                if (searchText.includes(key)) {
                    console.warn("Special Key <" + key + "> found in the following text...");
                    console.warn(text);
                    console.warn(current);
                    isFound = true;
                }
            });
        }
        
        if (current == headerElement) {
            return isFound;
        }
        current = current.previousSibling;
    }
    return isFound;
}

function loadWikipediaPage(wiki, callback) {
    var apiUrl = "https://en.wikipedia.org/w/api.php?origin=*&action=parse&format=json&page=" + wiki;
    console.log("Loading Wikipedia url <" + apiUrl + ">...");
    
    fetch(apiUrl, {
        method: "GET",
        cache: 'no-cache',
    })
    .then(response => response.json())
    .then(json => {
        //console.log("Response received...");
        callback(json);
    });
}